
const recordAudio = () =>
    new Promise(async resolve => {
        const stream = await navigator.mediaDevices.getUserMedia({ audio: true }); //it return a promise that resolves to a mediastream object.
        const mediaRecorder = new MediaRecorder(stream); // provide functionality to easily record media 
        //mediastream interface represent a stream of media content . A stream consists of several tracks such as videa nad audio
        const audioChunks = [];

        mediaRecorder.addEventListener('dataavailable', event => {
            audioChunks.push(event.data);
        });

        const start = () => mediaRecorder.start();

        const stop = () =>

            new Promise(async resolve => {
                mediaRecorder.addEventListener('stop', () => {

                    const audioblob = new Blob(audioChunks);
                    // const audioChunks = [];
                    const audioURL = URL.createObjectURL(audioblob);
                    const audio = new Audio(audioURL);
                    const play = () => audio.play();
                    resolve({ audioblob, audioURL, play });

                });

                mediaRecorder.stop();

                // set the mediarecorder.state to "inactive" and stop capturing media raise a dataavailable evenet containing the bolb of data that has been gathered 
            });

        resolve({ start, stop });

    });


const later = (delay, value) => {
    let timer = 0;
    let reject = null;
    const promise = new Promise((resolve, _reject) => {
        reject = _reject;
        timer = setTimeout(resolve, delay, value);
    });

    return {
        get promise() {

            return promise;

        },


        cancel() {
            if (timer) {
                clearTimeout(timer);
                timer = 0;
                reject();
                reject = null;
            }

        }
    };
};

const sleep = time => new Promise(resolve => setTimeout(resolve, time));

var audio = "";
var recorder = "";
const handleAction = async () => {

    const data_rep = document.getElementById('loop-number-input').value * 1000;
    const actionButton = document.getElementsByClassName('record_button_inside');
    const color_change = document.getElementsByClassName('record_button_inside')[0];
    const actionButton_p = document.getElementsByClassName('state_info')[0];
   
    actionButton.disabled = true;
    color_change.style.background = 'red';
    actionButton_p.innerText = "RECORDING";
  
    var element = document.getElementById('myBar');
    var width = 1;
    recorder = await recordAudio(); 
    recorder.start(); 
    // var id = setInterval(frame, document.getElementById('loop-number-input').value * 10); 
    await sleep(data_rep);   
         
    // this is display bar to show how much time left for recording

    function frame() {
        if (width >= 100) {
            console.log("entered");const recordAudio = () =>
    new Promise(async resolve => {
        const stream = await navigator.mediaDevices.getUserMedia({ audio: true }); //it return a promise that resolves to a mediastream object.
        const mediaRecorder = new MediaRecorder(stream); // provide functionality to easily record media 
        //mediastream interface represent a stream of media content . A stream consists of several tracks such as videa nad audio
        const audioChunks = [];

        mediaRecorder.addEventListener('dataavailable', event => {
            audioChunks.push(event.data);
        });

        const start = () => mediaRecorder.start();

        const stop = () =>

            new Promise(async resolve => {
                mediaRecorder.addEventListener('stop', () => {

                    const audioblob = new Blob(audioChunks);
                    const audioURL = URL.createObjectURL(audioblob);
                    const audio = new Audio(audioURL);
                    const play = () => audio.play();
                    const stop_playing =() =>{delete(audioblob); delete(audioURL) , delete(audio)};
                    resolve({ audioblob, audioURL, play , stop_playing});
                  
                    
                });

                mediaRecorder.stop();

                // set the mediarecorder.state to "inactive" and stop capturing media raise a dataavailable evenet containing the bolb of data that has been gathered 
            });

        resolve({ start, stop });

    });


const later = (delay, value) => {
    let timer = 0;
    let reject = null;
    const promise = new Promise((resolve, _reject) => {
        reject = _reject;
        timer = setTimeout(resolve, delay, value);
    });

    return {
        get promise() {

            return promise;

        },


        cancel() {
            if (timer) {
                clearTimeout(timer);
                timer = 0;
                reject();
                reject = null;
            }

        }
    };
};

const sleep = time => new Promise(resolve => setTimeout(resolve, time));
const handleAction = async () => {
    console.log("entry");
    // console.log(first_play);
    const data_rep = document.getElementById('loop-number-input').value * 1000;
    const actionButton = document.getElementsByClassName('record_button_inside');
    const color_change = document.getElementsByClassName('record_button_inside')[0];
    const actionButton_p = document.getElementsByClassName('state_info')[0];
   
    actionButton.disabled = true;
    color_change.style.background = 'red';
    actionButton_p.innerText = "RECORDING";
  
    var element = document.getElementById('myBar');
    var width = 1;
   
    console.log("first_time");
    const recorder = await recordAudio(); 
    recorder.start(); 
    const  record_bar_timer = setInterval(frame, document.getElementById('loop-number-input').value * 10); 
    element.style.background = '#4CAF50';
    actionButton_p.innerText = "Record !!!!";

    await sleep(data_rep); 
    const audio = await recorder.stop();
   
    function frame() {

        if (width >= 100) {
            console.log("entered_green_record");
            clearInterval(record_bar_timer);
            width = 1;
        } else {
            width++;
            element.style.width = width + '%';
            console.log(element.style.width);

        }
    }
    audio.play();
    width = 1 ;
    var play_bar_time = setInterval(frame_after, document.getElementById('loop-number-input').value * 10);
    await sleep(data_rep); 
    audio.stop_playing();
    console.log("enter dragon");
    function frame_after() {
        var element = document.getElementById('myBar');
        element.style.background = 'red';
        console.log("entered_play_100");
        

        if (width >= 100) {
            clearInterval(play_bar_time);
            console.log("entered_play_red_100");
            element.style.width = 1;

        } 

        else {

            console.log(element.style.width);
            width++;
            element.style.width = width + '%';

        }
    }


    width = 1;
    actionButton_p.innerText = "playing !!!!";
    // const l2 = await later(200 , 'l2') 
    color_change.style.background = 'inherit';
    actionButton.disabled = false;
    
   
    
    await handleAction();  
    

}



            clearInterval(id);
            width = 1;
        } else {
            width++;
            element.style.width = width + '%';

        }
    }

    audio = await recorder.stop();
    audio.play();
    // var id2 = setInterval(frame_after, document.getElementById('loop-number-input').value * 9.5);


    // this is bar to display the percentage of sound played 
    function frame_after() {
        var element = document.getElementById('myBar');
        element.style.background = 'red';
       
        if (width >= 100) {
            console.log("entered");
            clearInterval(id2);
            element.style.width = 1;

        } else {

            width++;
            element.style.width = width + '%';

        }
    }


    actionButton_p.innerText = "playing !!!!";
    color_change.style.background = 'inherit';
    actionButton.disabled = false;
    // if(document.getElementById('loopcheck').checked){
    return await handleAction();

    // }

}